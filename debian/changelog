ruby-narray (0.6.1.2-6) unstable; urgency=medium

  * Add patch: defining the allocator of T_DATA class NArray

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Tue, 04 Feb 2025 18:05:03 +0900

ruby-narray (0.6.1.2-5) unstable; urgency=medium

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Update standards version to 4.6.1, no changes needed.

  [ Youhei SASAKI ]
  * Remove X?-Ruby-Versions fields from d/control
  * Bump Standards-Version to 4.7.0 (no changes needed)
  * Drop obsolete ruby depends

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Fri, 22 Nov 2024 15:29:17 +0900

ruby-narray (0.6.1.2-4) unstable; urgency=medium

  [ Cédric Boutillier ]
  * [ci skip] Update team name
  * [ci skip] Add .gitattributes to keep unwanted files
              out of the source package

  [ Debian Janitor ]
  * Use secure URI in Homepage field.
  * Update watch file format version to 4.
  * Bump debhelper from old 12 to 13.
  * Drop transition for old debug package migration.
  * Update standards version to 4.5.0, no changes needed.
  * Update standards version to 4.5.1, no changes needed.

  [ Youhei SASAKI ]
  * Bump Standards-Version to 4.6.0 (no changes needed)
  * lintian-brush:
    - Remove empty leading paragraph in Description.
    - Fix field name cases in debian/control

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Wed, 20 Oct 2021 12:00:07 +0900

ruby-narray (0.6.1.2-3) unstable; urgency=medium

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Youhei SASAKI ]
  * d/control: update Vcs-{Git,Browser}. use salsa
  * d/{control,compat}: use debhelper-compat
  * d/control: Bump Standard Version 4.4.1
  * d/copyright: update uri, use https
  * d/watch: use gemwatch as upstream

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Sat, 18 Jan 2020 13:31:05 +0900

ruby-narray (0.6.1.2-2) unstable; urgency=medium

  * d/changelog: remove trailing whitespace
  * d/copyright: fix insecure-copyright-format-uri
  * d/copyright: update year
  * d/control: dbg package migration
  * Bump Standard Version: 4.1.3
  * Bump compat: 11

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Thu, 22 Feb 2018 03:13:09 +0900

ruby-narray (0.6.1.2-1) unstable; urgency=medium

  [ Cédric Boutillier ]
  * Use https:// in Vcs-* fields
  * Bump Standards-Version to 3.9.7 (no changes needed)
  * Run wrap-and-sort on packaging files

  [ Youhei SASAKI ]
  * New upstream version 0.6.1.2
  * Bump debhelper >= 10
  * Bump Standard Version: 4.0.0
  * Update debian/changelog
  * Update Maintainer/Uploaders (Closes: #863776)

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Thu, 31 Aug 2017 04:20:13 +0900

ruby-narray (0.6.1.1-2) unstable; urgency=medium

  * Bump Compat: 9

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Thu, 03 Sep 2015 21:02:34 +0900

ruby-narray (0.6.1.1-1) unstable; urgency=medium

  [ Caitlin Matos ]
  * updated homepage

  [ Youhei SASAKI ]
  * Imported Upstream version 0.6.1.1
  * Rename documents: follow upstream renaming
  * Update debian/rules: follow upstream structure
  * Bump Standard Version: 3.9.6
  * Update copyright format: dep5

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Sun, 22 Feb 2015 11:12:22 +0900

ruby-narray (0.6.0.8-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Update debian/watch to use rubygems.org

 -- Christian Hofstaedtler <zeha@debian.org>  Mon, 14 Apr 2014 23:08:36 +0200

ruby-narray (0.6.0.1-4) unstable; urgency=medium

  * Bump Standard Version: 3.9.5
  * Already Fixed linking issue (Closes: #723527)

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Thu, 30 Jan 2014 03:26:34 +0900

ruby-narray (0.6.0.1-3) unstable; urgency=medium

  * Drop obsolete lintian-overrides
  * Rebuild against Ruby >= 2.0

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Tue, 10 Dec 2013 01:50:12 +0900

ruby-narray (0.6.0.1-2) unstable; urgency=low

  [ Cédric Boutillier ]
  * debian/control: remove obsolete DM-Upload-Allowed flag
  * use canonical URI in Vcs-* fields

  [ Youhei SASAKI ]
  * Drop obsolete transitional packages
  * Fix Vcs-Git URL
  * debian/watch: remove dh_make templates
  * debian/docs: install SPEC{.ja,.en}
  * Build against newer gem2deb

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Wed, 11 Sep 2013 14:35:46 +0900

ruby-narray (0.6.0.1-1) unstable; urgency=low

  * New upstream release.

 -- Daigo Moriwaki <daigo@debian.org>  Sat, 08 Oct 2011 20:55:11 +0900

ruby-narray (0.5.9p9-1) unstable; urgency=low

  [ Daigo Moriwaki ]
  * New upstream release.
  * Moved to the new pkg-ruby-extras team's git repository.
  * Changed the package names to match the new Debian Policy.
    (very thanks to Youhei SASAKI)

  [ Youhei SASAKI ]
  * Bump Standard-Version: 3.9.2
  * Converted the build system to gem2deb, removed build dependency on cdbs
    and ruby-pkg-tools
  * debian/control:
    - Added XS-Ruby-Versions field
    - Added DM-Upload-Allowed: yes
    - Added transitional packages in debian/control to allow smooth upgrades
      from Squeeze.
  * debian/source/format: using '3.0 (quilt)'
  * Add source.lintian-overrides for *-dbg package transition.

 -- Daigo Moriwaki <daigo@debian.org>  Tue, 17 May 2011 00:56:40 +0900

libnarray-ruby (0.5.9p7-1) unstable; urgency=low

  [ Daigo Moriwaki ]
  * New upstream release.
  * debian/control:
    - Bumped up Standards-Version to 3.8.2.
    - Provide libnarray-ruby1.9.1 for Ruby 1.9.1.
    - Provide libnarray-ruby{1.8|1.9.1}-dbg.

  [ Lucas Nussbaum ]
  * Fixed Vcs-* fields after pkg-ruby-extras SVN layout change.

 -- Daigo Moriwaki <daigo@debian.org>  Thu, 20 Aug 2009 19:05:08 +0900

libnarray-ruby (0.5.9p6-1) unstable; urgency=low

  * New upstream release.
  * debian/control:
    - Bumped up Standards-Version to 3.8.1.
    - libnarray-ruby depends ${misc:Depends}.
    - Added Vcs-Svn and Vcs-Browser entries.

  * debian/compat: Bumped up to V7.

 -- Daigo Moriwaki <daigo@debian.org>  Sun, 14 Jun 2009 22:16:12 +0900

libnarray-ruby (0.5.9p5-2) unstable; urgency=low

  [ Gunnar Wolf ]
  * Changed section to Ruby as per ftp-masters' request

  [ Daigo Moriwaki ]
  * debian/watch: Now follow a correct package version.

 -- Daigo Moriwaki <daigo@debian.org>  Sun, 14 Jun 2009 22:14:46 +0900

libnarray-ruby (0.5.9p5-1) unstable; urgency=low

  * New upstream release.

 -- Daigo Moriwaki <daigo@debian.org>  Sat, 14 Jun 2008 00:39:56 +0900

libnarray-ruby (0.5.9p4-1) unstable; urgency=low

  * New upstream release.
  * debian/copyright: corrected the copyright style.

 -- Daigo Moriwaki <daigo@debian.org>  Thu, 06 Mar 2008 12:32:41 +0900

libnarray-ruby (0.5.9p3-1) unstable; urgency=low

  * New upstream release.
  * debian/control
    - Bump up Standards-Version to 3.7.3.
    - Use Homepage dpkg header.
  * debian/rules: Removed an unused variable, DEB_TAR_SRCDIR.
  * Removed unnecessary debian/dir.

 -- Daigo Moriwaki <daigo@debian.org>  Sun, 06 Jan 2008 15:44:43 +0900

libnarray-ruby (0.5.9p2-1) unstable; urgency=low

  * New upstream release.

 -- Daigo Moriwaki <daigo@debian.org>  Fri, 21 Dec 2007 12:39:17 +0900

libnarray-ruby (0.5.9-2) unstable; urgency=low

  * Stopped using the "Uploaders rule".

 -- Daigo Moriwaki <daigo@debian.org>  Wed, 13 Sep 2006 13:08:36 +0900

libnarray-ruby (0.5.9-1) unstable; urgency=low

  * New upstream release.
  * Fixed debian/watch. Thanks to Paul van Tilburg.

 -- Daigo Moriwaki <daigo@debian.org>  Wed, 13 Sep 2006 12:49:13 +0900

libnarray-ruby (0.5.8-1) unstable; urgency=low

  * Initial release. (Closes: #370120)

 -- Daigo Moriwaki <daigo@debian.org>  Sat,  3 Jun 2006 20:45:57 +0900
